package com.leetCode.chenyi.java;

public class ClassB extends ClassA {

    public static String classB_static ="classB_static_attr";

    public String classB_no_static ="classB_no_static_attr";

    static {
        System.out.println("ClassB_static_method");
    }

    {
        System.out.println(classB_no_static);
        System.out.println("ClassB_no_static_method");
    }

    public ClassB() {
        System.out.println("ClassB_constracter");
        System.out.println(classB_static);
        this.classB_no_static = classB_no_static;
    }

    public static void main(String[] args) {
        ClassB classB = new ClassB();
    }

}
