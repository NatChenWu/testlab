package com.leetCode.chenyi.java;

public class ClassA {
    public  static String classA_static ="classA_static_attr";

    public String classA_no_static ="classA_no_static_attr";

    static {
        System.out.println("ClassA_static_method");
    }

    {
        System.out.println(classA_no_static);
        System.out.println("ClassA_no_static_method");
    }

    public ClassA() {
        System.out.println("ClassA_constracter");
        System.out.println(classA_static);
        this.classA_no_static = classA_no_static;
    }
}
