package com.leetCode.chenyi.java.lock;

public  class LockAndSynchronized {

    private Object object = new Object();
    //用法1: 修饰方法,表示此方法是一个同步方法.
    //synchronized在修饰方法时,锁对象是(此此方法在经过编译后,多出来的synchronize-method关键字)
    //作用范围是 此方法
    public synchronized void method(){

    }
    //用法2: 修饰静态方法
    //此时synchronized 的锁是当前类对象.
    //作用范围是: 因为锁的当前的类对象,因为所有类实例在调用时都需要尝试获取锁
    public static  synchronized  void  method2(){

    }

    public void method3(){
        //用法3: 修饰代码块, 此处object作为锁对象,来保证线程运行到此处时后去唯一的锁.
        synchronized (object){
            System.out.println("我获取到锁啦");
            System.out.println("do xxxxxx");
            System.out.println("我执行完啦");
        }
        //作用范围:  由于锁对象是本类中的object,所以,锁范围是本类
    }


}
