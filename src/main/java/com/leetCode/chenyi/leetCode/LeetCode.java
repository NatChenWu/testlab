package com.leetCode.chenyi.leetCode;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import org.junit.jupiter.api.Test;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

public class LeetCode {

    public static void main(String[] args) {

        ListNode listNode = new ListNode(1);
        listNode.next = new ListNode(6);
        listNode.next.next= new ListNode(5);
        ListNode listNode1 = new ListNode(1);
        listNode1.next = new ListNode(2);
        listNode1.next.next= new ListNode(3);
        listNode1.next.next.next = new ListNode(4);
        mergeTwoLists(listNode,listNode1);


    }


     static class ListNode {
      int val;
      ListNode next;
       ListNode(int x) { val = x; }
     }
    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        List<Integer> list = new ArrayList<>();

        while (l1 !=null || l2 != null){
            if (l1 == null && l2 ==null){
                break;
            }
            if(l1!=null) {
                list.add((l1.val ));
                l1 = l1.next;
            }
            if (l2!=null) {
                list.add((l2.val));
                l2 = l2.next;
            }


        }
        list.sort(Comparator.comparingInt(Integer::intValue));

        ListNode merge = null;
        for (int i = list.size()-1; i >=0; i--) {
            if (i == list.size()-1){
                ListNode end = new ListNode(list.get(i));
                merge = end;
            }else {
                ListNode listNode = new ListNode(list.get(i));
                listNode.next =merge;
                merge = listNode;
            }

        }

        while (merge!=null){
            System.out.println(merge.val);
            merge = merge.next;
        }


        return null;
    }

    public static boolean isValid(String s) {
        if ("".equals(s)|| "".equals(s.trim())){
            return true;
        }

        while (true){

            if (s.contains("()") || s.contains("[]") || s.contains("[]")){
                s = s.replace("{}", "").replace("()", "").replace("[]", "");
                if(StringUtils.isEmpty(s)){
                    return true;
                }
            }else {
                return false;
            }
        }
    }

    public static String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) return "";
        for (int i = 0; i < strs[0].length() ; i++){
            char c = strs[0].charAt(i);
            for (int j = 1; j < strs.length; j ++) {
                if (i == strs[j].length() || strs[j].charAt(i) != c)
                    return strs[0].substring(0, i);
            }
        }
        return strs[0];
    }




}
