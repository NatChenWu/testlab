package com.leetCode.chenyi.leetCode;

import java.math.BigDecimal;

public class DoTest {

    public static void main(String[] args) {
        ListNode listNode = new ListNode(2);
        ListNode listNode1 = new ListNode(4);
        ListNode listNode2 = new ListNode(3);
        listNode1.next =listNode2;
        listNode.next =listNode1;

        ListNode listNode3 = new ListNode(5);
        ListNode listNode4 = new ListNode(6);
        ListNode listNode5 = new ListNode(4);
        listNode4.next =listNode5;
        listNode3.next =listNode4;

        ListNode listNode6 = addTwoNumbers(listNode, listNode3);

        while (listNode6!=null){
            System.out.println(listNode6.val);
            listNode6 =listNode6.next;
        }


    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        String l1str = "";
        while (l1!=null){
            l1str =l1str+l1.val;
            l1 =l1.next;
        }
        String l2str ="";
        while (l2!=null){
            l2str = l2str+l2.val;
            l2= l2.next;
        }
        int value = 0;
        try {
            BigDecimal add = new BigDecimal(l1str).add(new BigDecimal(l2str));
            if (add.compareTo(new BigDecimal(Integer.MAX_VALUE)) > 0){
                return  new ListNode(0);
            }else {
                value = add.intValue();
            }
        } catch (NumberFormatException e) {
            return new ListNode(0);
        }
        char[] chars = String.valueOf(value).toCharArray();
        ListNode listNode = null;

        System.out.println(value);
        for (char aChar : chars) {
            ListNode node = new ListNode(Integer.valueOf(aChar+""));
            node.next=listNode;
            listNode =node;
        }
        return listNode;

    }

}


   class ListNode {
      int val;
      ListNode next;

      ListNode(int x) {
          val = x;
      }
  }
